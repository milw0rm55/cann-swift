//
//  MapViewController.swift
//  Pimpam
//
//  Created by SEBASTIÁN TANGARIFE ACERO on 3/12/18.
//  Copyright © 2018 SEBASTIÁN TANGARIFE ACERO. All rights reserved.
//

import UIKit
import MapKit
class MapViewController: UIViewController {
    @IBOutlet weak var mapView : MKMapView!
    @IBOutlet weak var segmentedControl : UISegmentedControl!
    internal var locationTiShow: CLLocation?
    
    convenience init (lat:Double!, lng:Double!){
        self.init()
        self.locationTiShow = CLLocation(latitude: lat, longitude: lng)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = (locationTiShow?.coordinate)!
        mapView.addAnnotation(annotation)
        
        let region = MKCoordinateRegionMakeWithDistance((locationTiShow?.coordinate)!, 2000, 2000)
        mapView.setRegion(region, animated: true)
    }
    
    @IBAction func segmentedControlTypeDidChange(){
        switch  segmentedControl.selectedSegmentIndex {
        case 0:
            mapView.mapType = .standard
        case 1:
            mapView.mapType = .satellite
        case 2:
            mapView.mapType = .hybrid
        default:
            mapView.mapType = .standard
        }
    }
}
