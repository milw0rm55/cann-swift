//
//  RecipeTextViewController.swift
//  Pimpam
//
//  Created by SEBASTIÁN TANGARIFE ACERO on 29/11/18.
//  Copyright © 2018 SEBASTIÁN TANGARIFE ACERO. All rights reserved.
//

import UIKit

class RecipeTextViewController: UIViewController {
    var recypes:Recipes!
    @IBOutlet weak var imagerecipe : UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var difficultlbl: UILabel!
    @IBOutlet weak var descriptionlbl: UILabel!
    
    @IBAction func showMaps(){
        let mapVC = MapViewController(lat: recypes.lat, lng: recypes.lon)
        navigationController?.pushViewController(mapVC, animated: true)
    }
    
    convenience init(recypes:Recipes){
        self.init()
        self.recypes = recypes
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
