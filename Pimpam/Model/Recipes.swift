//
//  Recipes.swift
//  Pimpam
//
//  Created by SEBASTIÁN TANGARIFE ACERO on 28/11/18.
//  Copyright © 2018 SEBASTIÁN TANGARIFE ACERO. All rights reserved.
//

import Foundation
class Recipes {
    var name: String!
    var difficult: String!
    var foodrecipesimg: String!
    var description : String!
    var lat : Double!
    var lon : Double!
    
    init(name: String, difficult: String, foodrecipesimg: String, description: String, lat: Double, lon: Double) {
        self.name = name
        self.difficult = difficult
        self.foodrecipesimg = foodrecipesimg
        self.description = description
        self.lat = lat
        self.lon = lon
    }
}
